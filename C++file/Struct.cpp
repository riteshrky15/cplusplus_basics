#include<iostream>
#include<string>
using namespace std;

int main() //Intialize my main function
{
	struct // intialize my structure
	{
		string brand;
		string model;
		int year;
	} iphone,note; // create 2 variables (members)
	
	//Append value to member 
	iphone.brand = "Apple";
	iphone.model= "13";
	iphone.year = 2022;
	
	note.brand = "Samsung";
	note.model = "20";
	note.year = 2021;
	
	// Print whatever you want
	
	cout<<iphone.brand<<"\n";
	cout<<note.model<<"\n";
	
	return 0;
}
