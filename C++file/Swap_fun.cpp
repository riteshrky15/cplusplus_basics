// refrences in function 
#include<iostream>
#include<string>
using namespace std;

void SwapNums(int &x,int &y)
{
	int z = x;
	x=y;
	y=z;
}
int main()
{
	int firstN=10;
	int doubleN = 20;
	cout<<"Before swap  "<< "\n";
	cout<< "First num = "<< firstN<<"\n";
	cout<< "Second Num = "<< doubleN<<"\n";
	
	SwapNums(firstN,doubleN);
	cout<<"After Swap "<<"\n";
	cout<<"First num = "<<firstN<<"\n";
	cout<< "Second Num = "<<doubleN<<"\n";
}
