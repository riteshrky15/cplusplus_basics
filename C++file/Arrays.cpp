//Arrays can store multiple values 
// Syntax ---> Datatype Array_Name[no. of items];
//Syntax --> Datatype Array_Name[No. of items ] = {vol1,vol2,vol3.....,volN};
#include<iostream>
#include<string>
using namespace std;

int main()
{
	string brand[4] = {"Puma","Rebook","Nike","Addidas"};
	int myNum[3] = {1,2,3};
	cout<<brand[2]<<"\n";
	cout<<myNum[1]<<"\n";
	brand[2]= "Playboy";
	cout<<brand[2]<"\n";
	return 0;

}

