// structures (struct) are used to group several related variables
// Every object that uses structure is called memeber of it 
// Unlike array structure can have diffrenet data types
// struct //Initialization
//int mynumber //Member integer called mynumber
// string mystring // Memeber string called by mystring 
// mystructure  // variable that camn access all memeber of this structure 
#include<iostream>
#include<string>
using namespace std;

struct Car
{
	string brand ;
	string model;
	int year;
	
};

int main()
{
	Car car1;
	car1.brand = "Maruti";
	car1.model = "swift";
	car1.year = 2020;
	
	Car car2;
	car2.brand = "Hyundai";
	car2.model = "i20";
	car2.year = 2021;
	
	//print the structure member for our variables car1 and car2
	cout<<car1.brand<<" "<<car1.model<< " "<< car1.year<<"\n";
	cout<<car2.brand<<" "<<car2.model<< " "<< car2.year<<"\n";
	
}
