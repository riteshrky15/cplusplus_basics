// Pointer in C++ ----> Created using "*" followed by variable name 
#include<iostream>
#include<string>
using namespace std;

int main()
{
//	string meal = "Burger";// All variable with value 'burger' stored in it 
//	string* pointer = &meal;// Apointer variable with the name 'pointer' that stores the memory loaction of the variable meal
//	
//	cout<<meal<< "\n"; // ---> "Burgers"
//	cout <<&meal<<"\n";// ---> meomory Location of meal
//	cout<< pointer;//---> it should also be the  memory Location of meal
string food = "pizza";// created a variable by the name food and stored value pizza 
string* ptr = &food; // declaring a pointer to variable food 

// Refrence : The below line will output the memory ofthe variable food 

cout<<ptr<<"\n";//Refrence : the below line will output the memory of vaiable food

cout<<*ptr<<"\n"; //Location of variable	
	return 0;
	
}
