//Refrences in C++ -----> created using '&' followed by a variable name
#include<iostream>
#include<string>
using namespace std;

int main()
{
	string dress_code = "formals"; //variable by the name dress_code
    string &attire = dress_code; // refrence to dress_code
    
    cout<<dress_code<<" \n";
    cout<<attire;
    
    return 0;
    
    
}
