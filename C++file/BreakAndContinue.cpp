// Break And Continue in C++
#include<iostream>//header file
using namespace std;//Standard namspace


//int main()
//{
//	for(int i=0;i<10;i++)
//	{
//		if(i==4)
//		{
//			break;
//		}
//		cout<<i<<"\n";
//	}
//	return 0;
//}
int main()
{
	for(int i =0;i<10;i++)
	{
		if(i==4)
		{
			continue;
		}
		cout<<i<<"\n";
	}
	return 0;
}
