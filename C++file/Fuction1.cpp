//.............FUNCTION IN C++>..................
// what is a function : Ablock of code (part of code ) which runs or execute only when called for
//---> Function can accept inputs and also return outputs 
//----> primary logic of function is to process something on your input 
//----> they are extremly useful in resuming for another programs which might need them
// ---->Ex : We create a function in any program by copy ___paste into it 
//... we can use this same function any program
//-- Function _---> string to text (string input):::::---> file1.cpp
#include<iostream>
#include<string>
using namespace std;

void func()
{
	cout<<"I am go through function!"<<"\n";
}
int main()// starting point 
{
	func();// call the function
	cout<<"Now it's done";
	return 0;
}
